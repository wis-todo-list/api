<?php
namespace App\libraries;

use Firebase\JWT\JWT;
use Tuupola\Base62;

class Tokenizer {

  const ALGORITHM = 'HS256';
  private $secretKey;
  private $issuedAt;
  private $expireAt;
  private $tokenId;
  private $payload = [];

  public function __construct () {
    $this->secretKey = getenv('APP_KEY');
  }

  public function createToken ($additionalPayload) {
    $now = new \DateTime();
    $future = new \DateTime('+1 hour');

    $this->issuedAt = $now->getTimeStamp();
    $this->expireAt = $future->getTimeStamp();
    $this->tokenId = (new Base62)->encode(random_bytes(16));
    $this->payload['iat'] = $this->issuedAt;
    $this->payload['exp'] = $this->expireAt;
    $this->payload['jti'] = $this->tokenId;

    if (is_array($additionalPayload)) {
      foreach ($additionalPayload as $key => $value) {
        $this->payload[$key] = $value;
      }
      unset($key);
      unset($value);
    }

    $token = JWT::encode($this->payload, $this->secretKey, $this::ALGORITHM);

    return $token;
  }

  public function decodeToken () {

  }

}
