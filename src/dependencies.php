<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
  $settings = $c->get('settings')['renderer'];
  return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
  $settings = $c->get('settings')['logger'];
  $logger = new Monolog\Logger($settings['name']);
  $logger->pushProcessor(new Monolog\Processor\UidProcessor());
  $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
  return $logger;
};

// Eloquent service factory
$container['db'] = function ($c) {
  $capsule = new \Illuminate\Database\Capsule\Manager;
  $settings = $c->get('settings')['db'];
  $capsule->addConnection($settings);
  $capsule->setAsGlobal();
  $capsule->bootEloquent();
  $capsule->getContainer()->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class
  );
  return $capsule;
};

// instantiated controllers factory
$container['UsersController'] = function ($c) {
  return new App\controllers\UsersController($c->get('settings'));
};
$container['TasksController'] = function ($c) {
  return new App\controllers\TasksController($c->get('settings'));
};
