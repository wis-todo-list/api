<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$container = $app->getContainer();

$container['jwt'] = function ($c) {
  return new StdClass;
};

$app->add(new \Slim\Middleware\JwtAuthentication([
  'path'   => '/',
  'logger' => $container['logger'],
  'secret' => getenv('APP_KEY'),
  'algorithm' => 'HS256',
  'secure' => false,
  'rules'  => [
    new \Slim\Middleware\JwtAuthentication\RequestPathRule([
      'path' => '/',
      'passthrough' => ['/users/login', '/users/register']
    ]),
    new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
      'passthrough' => ['OPTIONS']
    ])
  ],
  'callback' => function ($request, $response, $args) use ($container) {
    $container['jwt'] = $args['decoded'];
  },
  'error' => function ($request, $response, $args) {
    $data = [
      'status' => [
        'code' => 401,
        'text' => 'Unauthorized',
        'message' => $args['message']
      ],
      'body' => []
    ];
    return $response->withStatus($data['status']['code'])
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
  }
]));

$app->add(new \Tuupola\Middleware\Cors([
  'logger'  => $container['logger'],
  'origin'  => ['*'],
  'methods' => ['GET', 'POST', 'PUT', 'DELETE'],
  'headers.allow'  => ['Authorization', 'If-Match', 'If-Unmodified-Since'],
  'headers.expose' => ['Authorization', 'Etag'],
  'credentials'    => true,
  'cache'          => 60,
  'error'          => function ($request, $response, $args) {
    return new UnauthorizedResponse($args['message'], 401);
  }
]));
