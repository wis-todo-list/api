<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Users routes
$app->group('/users', function () {
  // Login
  $this->post('/login', 'UsersController:login')->setName('login');
  // Logout
  $this->post('/logout', 'UsersController:logout')->setName('logout');
  // Register
  $this->put('/register', 'UsersController:register')->setName('register');
  // Get details
  $this->get('[/{userId:[0-9]+}]', 'UsersController:index')->setName('index');
});

// Tasks routes
$app->group('/tasks', function () {
  // Create
  $this->put('/create', 'TasksController:create')->setName('task.create');
  // Update
  $this->patch('/update/{taskId:[0-9]+}', 'TasksController:update')->setName('task.update');
  // Delete
  $this->delete('/delete/{taskId:[0-9]+}', 'TasksController:delete')->setName('task.delete');
  // Indexing
  $this->get('/by-user/{userId:[0-9]+}', 'TasksController:index')->setName('task.index');
});

// $app->get('/[{name}]', function (Request $request, Response $response, array $args) {
//     // Sample log message
//     $this->logger->info("Slim-Skeleton '/' route");
//
//     // Render index view
//     return $this->renderer->render($response, 'index.phtml', $args);
// });
