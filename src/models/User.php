<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

  protected $table = 'users';
  protected $fillable = [
    'person_name',
    'username',
    'password_hash',
    'email',
  ];

}
