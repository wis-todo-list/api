<?php
namespace App\controllers;

use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use \App\models;

class TasksController extends Controller {

  private $model;

  public function __construct ($c) {
    parent::__construct($c);

    $this->model = new models\Task;
  }

  public function index (Request $request, Response $response, array $args) {
    $userId = (int)$args['userId'];
    $tasks = $this->model->where('user_id', '=', $userId)->get();

    if (count($tasks) > 0) {
      $body = [];

      foreach ($tasks as $key => $value) {
        $body[$key] = [
          'id' => $value->id,
          'name' => $value->name,
          'is_completed' => $value->is_completed
        ];
      }
      unset($key);
      unset($value);

      $data = [
        'status' => [
          'code' => 200,
          'text' => 'OK',
          'message' => 'Tasks successfully fetched.'
        ],
        'body' => $body
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
    else {
      $data = [
        'status' => [
          'code' => 204,
          'text' => 'No Content',
          'message' => 'Tasks successfully fetched with empty result.'
        ],
        'body' => []
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
  }

  public function create (Request $request, Response $response) {
    $body = $request->getParsedBody();
    $task = $body['taskName'];
    $userId = (int)$body['userId'];

    $this->model->user_id = $userId;
    $this->model->name = $task;
    $this->model->save();

    $data = [
      'status' => [
        'code' => 201,
        'text' => 'Created',
        'message' => 'Task successfully created.'
      ],
      'body' => [
        'task_id' => $this->model->id
      ]
    ];

    return $response->withStatus($data['status']['code'])
                    ->withHeader('Content-Type', 'application/json')
                    ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
  }

  public function update (Request $request, Response $response, array $args) {
    $body = $request->getParsedBody();
    $taskId = (int)$args['taskId'];
    $task = $this->model->find($taskId);

    if ($task) {
      $task->name = $body['taskName'];
      $task->save();

      $data = [
        'status' => [
          'code' => 204,
          'text' => 'No Content',
          'message' => 'Update successful.'
        ],
        'body' => []
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
    else {
      $data = [
        'status' => [
          'code' => 400,
          'text' => 'Bad Request',
          'message' => 'Update fail. Task is not found.'
        ],
        'body' => []
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
  }

  public function delete (Request $request, Response $response, array $args) {
    $taskId = (int)$args['taskId'];
    $task = $this->model->find($taskId);

    if ($task) {
      $task->delete();

      $data = [
        'status' => [
          'code' => 204,
          'text' => 'No Content',
          'message' => 'Update successful.'
        ],
        'body' => []
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
    else {
      $data = [
        'status' => [
          'code' => 400,
          'text' => 'Bad Request',
          'message' => 'Update fail. Task is not found.'
        ],
        'body' => []
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
  }
}
