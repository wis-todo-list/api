<?php
namespace App\controllers;

use Illuminate\Database\Query\Builder;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use \App\models;

class UsersController extends Controller {

  private $model;

  public function __construct ($c) {
    parent::__construct($c);

    $this->model = new models\User;
  }

  public function index (Request $request, Response $response, array $args) {
    return $response->withJson($this->model::all());
  }

  public function login (Request $request, Response $response) {
    $body = $request->getParsedBody();
    $loginBy = (filter_var($body['userIdentity'], FILTER_VALIDATE_EMAIL)) ? 'email' : 'username';

    $user = $this->model->where($loginBy, '=', $body['userIdentity'])
                        ->where('status', '<>', 2)
                        ->first();
    if ($user) {
      $password = $body['userPassword'];
      $passwordIsValid = $this->validatePassword($password, $user->password_hash);

      if ($passwordIsValid) {
        // authenticated, create JWT token
        $tokenizer = new \App\libraries\Tokenizer;
        $token = $tokenizer->createToken([
          'uid' => $user->id,
          'una' => $user->username
        ]);
        $data = [
          'status' => [
            'code' => 200,
            'text' => 'OK',
            'message' => 'Login successful.'
          ],
          'body' => [
            'token' => $token,
            'user' => [
              'id' => $user->id,
              'username' => $user->username,
              'person_name' => $user->person_name
            ]
          ]
        ];

        return $response->withStatus($data['status']['code'])
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
      }
      else {
        // password is invalid
        $data = [
          'status' => [
            'code' => 400,
            'text' => 'Bad Request',
            'message' => 'Login fail. Username or Password may be wrong.'
          ],
          'body' => []
        ];

        return $response->withStatus($data['status']['code'])
                        ->withHeader('Content-Type', 'application/json')
                        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
      }
    }
    else {
      // return user not found
      $data = [
        'status' => [
          'code' => 400,
          'text' => 'Bad Request',
          'message' => 'Login fail. Username or Password may be wrong.'
        ],
        'body' => []
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
  }

  public function logout (Request $request, Response $response) {}

  public function register (Request $request, Response $response) {
    $body = $request->getParsedBody();
    $username = $body['username'];
    $email = $body['email'];

    $userIsExist = $this->model->where('username', '=', $username)
                               ->orWhere('email', '=', $email)
                               ->first();
    if (!$userIsExist) {
      $this->model->username = $username;
      $this->model->email = $email;
      $this->model->password_hash = $this->makePasswordHash($body['password']);
      $this->model->save();

      $tokenizer = new \App\libraries\Tokenizer;
      $token = $tokenizer->createToken([
        'uid' => $this->model->id,
        'una' => $this->model->username
      ]);

      $data = [
        'status' => [
          'code' => 201,
          'text' => 'Created',
          'message' => 'Registration successful.'
        ],
        'body' => [
          'token' => $token,
          'user' => [
            'id' => $this->model->id,
            'username' => $this->model->username,
            'person_name' => $this->model->person_name
          ]
        ]
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
    else {
      $data = [
        'status' => [
          'code' => 400,
          'text' => 'Bad Request',
          'message' => 'Registration fail. User is exist.'
        ],
        'body' => []
      ];

      return $response->withStatus($data['status']['code'])
                      ->withHeader('Content-Type', 'application/json')
                      ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
  }

  protected function makePasswordHash (string $password) {
    $options = [
      'salt' => getenv('APP_KEY')
    ];
    return password_hash($password, PASSWORD_BCRYPT, $options);
  }

  protected function validatePassword (string $password, string $passwordHash) {
    return password_verify($password, $passwordHash);
  }

  protected function userIsValid (int $userId) {
    $user = $this->model->find($userId);
    return ($user->status === 1);
  }

}
