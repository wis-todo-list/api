<?php
return [
  'settings' => [
    // Slim settings
    'displayErrorDetails' => (bool)getenv('DISPLAY_ERRORS'), // set to false in production
    'addContentLengthHeader' => false, // Allow the web server to send the content-length header

    // DB settings
    'db' => [
      'driver'    => getenv('DB_DRIVER'),
      'host'      => getenv('DB_HOST'),
      'database'  => getenv('DB_NAME'),
      'username'  => getenv('DB_USERNAME'),
      'password'  => getenv('DB_PASSWORD'),
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prefix'    => '',
    ],

    // Renderer settings
    'renderer' => [
      'template_path' => __DIR__ . '/../templates/',
    ],

    // Monolog settings
    'logger' => [
      'name'  => getenv('LOGGER_NAME'),
      'path'  => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
      // 'level' => \Monolog\Logger::DEBUG,
      'level' => (int)getenv('LOG_LEVEL')
    ],
  ],
];
